#ifndef CLOSEST_POINT_HPP_INCLUDED
#define CLOSEST_POINT_HPP_INCLUDED

#include <optional>
#include <vector>

#include "glm/glm.hpp"

namespace ClosestPoint
{
    //
    // Basic geometry types
    //

    typedef glm::vec3 Point;

    struct Triangle
    {
        Point A, B, C;
    };

    struct BBox
    {
        Point minCoords, maxCoords;
    };

    //
    // Basic closest point querys
    //

    /// Finds the closest point in a triangle from a query point
    // Based on the algorithm described inside "Real-time collision detection"
    // by Christer Ericson.
    Point closestPointInTriangle(const Triangle &triangle, const Point &P);

    /// Finds the closest point in a bounding box from a query point
    // Based on the algorithm described inside "Real-time collision detection"
    // by Christer Ericson
    Point closestPointInBBox(const BBox &bbox, const Point &P);

    //
    // ClosestPointInMesh class
    //

    /// Class to efficiently compute the closest points in a mesh to a query point.
    // It uses an internal acceleration structure to speed up multiple
    // queries using a common mesh.
    class ClosestPointInMesh
    {
    public:
        template <typename TriangleIter>
        ClosestPointInMesh(TriangleIter begin, TriangleIter end)
            : _triangles(begin, end)
        {
            makeNode(_triangles.data(), _triangles.data() + _triangles.size());
        }

        ClosestPointInMesh(const std::vector<Triangle> &triangles)
            : ClosestPointInMesh(triangles.begin(), triangles.end())
        {
        }

        // Computes the closest point in the mesh to the point within the max distance
        std::optional<Point> operator()(const Point &P, float maxDistance = INFINITY) const
        {
            return _nodes.empty() ? std::nullopt : closestInNode(_nodes[0], P, maxDistance);
        }

    private:
        struct Node
        {
            // Bounding Box coordinates
            BBox bbox = {
                .minCoords = {INFINITY, INFINITY, INFINITY},
                .maxCoords = {-INFINITY, -INFINITY, -INFINITY}};

            // Index to childs in _nodes array or -1 if there is no child
            Node *childs[2] = {
                nullptr,
                nullptr};

            // Range of related triangles
            Triangle *begin = nullptr;
            Triangle *end = nullptr;
        };

        // Create a new node from a triangle range
        Node *makeNode(Triangle *begin, Triangle *end);

        // Computes the closest point in the mesh to the point within the max distance
        std::optional<Point> closestInNode(const Node &node, const Point &P, float &maxDistance) const;

        std::vector<Node> _nodes;
        std::vector<Triangle> _triangles;
    };

} // namespace ClosestPoint

#endif // not defined CLOSEST_POINT_HPP_INCLUDED
