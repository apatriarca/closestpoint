====================
ClosestPoint Library
====================

The ``ClosestPoint`` library has been implemented as an exercise. It implements a
class that can be used to find the closest point on a mesh (in fact any collection
of triangles) to a query point. The class assumes the client is interested in
computing multiple queries using the same input mesh. For this reason, it creates
an internal statial acceleration data structure to rapidly skip parts of the mesh.

The following code show an example usage:

.. code-block:: C++

    std::vector<Triangle> triangles;
    // ... Fill the `triangles` array with the mesh triangles
    ClosestPointInMesh query(triangles);

    Point p = // .. create query point
    auto cp = query(p);
    // cp.value now contains the closest point

It is possible to pass an additional arguments to limit the query search radius.

Assumptions, Limitations and Possible Improvements
--------------------------------------------------

1. The library only supports triangle meshes/collections. Supporting quads or higher 
   order polygons may be much more complicated as these may not be flat.
2. It does not try to be particularly portable. It is implemented in C++17. It can 
   be changed to support older versions of the C++ by removing a few newer minor new
   features I decided to use.
3. It has been implemented in Linux, but should be possible to compile it in any other
   modern OS.
4. I haven't spent much time optimizing the code and it is probably quite sub-optimal.
5. I haven't analysed the code for robustness issues.
6. Further study is probably required to verify the actual speed up in multiple test
   cases.

Dependencies
------------

The ``ClosestPoint`` library only depends on the `glm <https://glm.g-truc.net/0.9.9/index.html>`_ 
header only mathematics library. This dependency has been included in the library code, so
there is no need to download it separately.

The unit tests have a few additional dependencies. They are implemented using the 
`doctest framework <https://github.com/onqtam/doctest>`_ and they use the
`fast_obj <https://github.com/thisistherk/fast_obj>`_ library for loading the example
meshes. The are both composed of very few files and they are thus included directly
in the tests source folder. 

All the dependencies are licensed according to the MIT license.
