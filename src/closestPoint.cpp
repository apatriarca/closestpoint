#include <closestPoint.hpp>
#include <algorithm>

namespace ClosestPoint
{
    //
    // Basic querys
    //

    typedef glm::vec3 Vector;

    /// Finds the closest point in a triangle from a query point
    Point closestPointInTriangle(const Triangle &triangle, const Point &P)
    {
        // Check if P in vertex region outside A
        const Vector AB = triangle.B - triangle.A;
        const Vector AC = triangle.C - triangle.A;
        const Vector AP = P - triangle.A;
        const float d1 = glm::dot(AB, AP);
        const float d2 = glm::dot(AC, AP);
        if (d1 <= 0.0f && d2 <= 0.0f)
        {
            return triangle.A;
        }

        // Check if P in vertex region outside B
        const Vector BP = P - triangle.B;
        const float d3 = glm::dot(AB, BP);
        const float d4 = glm::dot(AC, BP);
        if (d3 >= 0.0f && d4 <= d3)
        {
            return triangle.B;
        }

        // Check if P in edge region of AB, if so return projection of P onto AB
        const float vc = d1 * d4 - d3 * d2;
        if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f)
        {
            const float v = d1 / (d1 - d3);
            return triangle.A + v * AB;
        }

        // Check if P in vertex region outside C
        const Vector CP = P - triangle.C;
        const float d5 = glm::dot(AB, CP);
        const float d6 = glm::dot(AC, CP);
        if (d6 >= 0.0f && d6 >= d5)
        {
            return triangle.C;
        }

        // Check if P in edge region of AC, if so return projection of P onto AC
        const float vb = d5 * d2 - d1 * d6;
        if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f)
        {
            const float w = d2 / (d2 - d6);
            return triangle.A + w * AC;
        }

        // Check if P in edge region of BC, if so return projection of P onto BC
        const float va = d3 * d6 - d5 * d4;
        const float s = d4 - d3;
        const float t = d5 - d6;
        if (va <= 0.0f && s >= 0.0f && t >= 0.0f)
        {
            const float w = s / (s + t);
            return glm::mix(triangle.B, triangle.C, w);
        }

        // P inside face region. Compute Q through its barycentric coordinates (u,v,w)
        const float denom = 1.0f / (va + vb + vc);
        const float v = vb * denom;
        const float w = vc * denom;
        return triangle.A + v * AB + w * AC;
    }

    // Finds the closest point in a bounding box from a query point
    Point closestPointInBBox(const BBox &bbox, const Point &P)
    {
        return glm::clamp(P, bbox.minCoords, bbox.maxCoords);
    }

    // Returns the range of distances of point in a box to a query point
    std::pair<float, float> distanceRangeInBox(const BBox &bbox, const Point &P)
    {
        const Point closestPoint = closestPointInBBox(bbox, P);
        const Point farthestPoint = glm::max(glm::abs(bbox.minCoords - P), glm::abs(bbox.maxCoords - P));

        return {glm::distance(closestPoint, P), glm::distance(farthestPoint, P)};
    }

    //
    // ClosestPointInMesh
    //

    const size_t LEAF_SIZE = 16;

    // Create a new node from a triangle range
    ClosestPointInMesh::Node *ClosestPointInMesh::makeNode(Triangle *begin, Triangle *end)
    {
        Node &n = _nodes.emplace_back();

        // Set triangles range
        n.begin = begin;
        n.end = end;

        // Compute min and max coordinates for bounding box
        auto &[minCoords, maxCoords] = n.bbox;
        for (Triangle *triangle = n.begin; triangle != n.end; ++triangle)
        {
            minCoords = glm::min(minCoords, triangle->A);
            maxCoords = glm::max(maxCoords, triangle->A);

            minCoords = glm::min(minCoords, triangle->B);
            maxCoords = glm::max(maxCoords, triangle->B);

            minCoords = glm::min(minCoords, triangle->C);
            maxCoords = glm::max(maxCoords, triangle->C);
        }

        if ((n.end - n.begin) <= LEAF_SIZE)
        {
            // Leaf node
            return &n;
        }

        // Choose axis to subdivide childs
        const Vector extents = maxCoords - minCoords;

        int maxAxis = 0;
        if (extents.x > extents.y)
        {
            if (extents.x > extents.z)
            {
                maxAxis = 0;
            }
            else
            {
                maxAxis = 2;
            }
        }
        else if (extents.y > extents.z)
        {
            maxAxis = 1;
        }
        else
        {
            maxAxis = 2;
        }

        if (maxAxis == 0)
        {
            std::sort(n.begin, n.end,
                      [](const Triangle &t0, const Triangle &t1) {
                          const float sumX0 = t0.A.x + t0.B.x + t0.C.x;
                          const float sumX1 = t1.A.x + t1.B.x + t1.C.x;
                          return sumX0 < sumX1;
                      });
        }
        else if (maxAxis == 1)
        {
            std::sort(n.begin, n.end,
                      [](const Triangle &t0, const Triangle &t1) {
                          const float sumY0 = t0.A.y + t0.B.y + t0.C.y;
                          const float sumY1 = t1.A.y + t1.B.y + t1.C.y;
                          return sumY0 < sumY1;
                      });
        }
        else if (maxAxis == 2)
        {
            std::sort(n.begin, n.end,
                      [](const Triangle &t0, const Triangle &t1) {
                          const float sumZ0 = t0.A.z + t0.B.z + t0.C.z;
                          const float sumZ1 = t1.A.z + t1.B.z + t1.C.z;
                          return sumZ0 < sumZ1;
                      });
        }

        Triangle *mid = n.begin + (n.end - n.begin) / 2;

        // Create childs
        n.childs[0] = makeNode(n.begin, mid);
        n.childs[1] = makeNode(mid, n.end);

        return &n;
    }

    // Computes the closest point in the mesh to the point within the max distance
    std::optional<Point> ClosestPointInMesh::closestInNode(const Node &n, const Point &P, float &maxDistance) const
    {
        // Early out based on bounding box
        const auto [minDist, maxDist] = distanceRangeInBox(n.bbox, P);
        if (minDist > maxDistance)
        {
            return std::optional<Point>();
        }

        if (!n.childs[0])
        {
            // Leaf node
            std::optional<Point> result;

            for (Triangle *triangle = n.begin; triangle != n.end; ++triangle)
            {
                Point cp = closestPointInTriangle(*triangle, P);
                float distance = glm::distance(cp, P);
                if (distance < maxDistance)
                {
                    result = cp;
                    maxDistance = distance;
                }
            }

            return result;
        }
        else
        {
            const auto [minDist0, maxDist0] = distanceRangeInBox(n.childs[0]->bbox, P);
            const auto [minDist1, maxDist1] = distanceRangeInBox(n.childs[1]->bbox, P);

            std::optional<Point> result;
            if (minDist0 < minDist1)
            {
                result = closestInNode(*n.childs[0], P, maxDistance);
                auto cp = closestInNode(*n.childs[1], P, maxDistance);
                if (cp)
                {
                    result = cp;
                }
            }
            else
            {
                result = closestInNode(*n.childs[1], P, maxDistance);
                auto cp = closestInNode(*n.childs[0], P, maxDistance);
                if (cp)
                {
                    result = cp;
                }
            }

            return result;
        }
    }

} // namespace ClosestPoint
