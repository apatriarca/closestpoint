#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>

#include <cmath>
#include <random>

#include <closestPoint.hpp>
using namespace ClosestPoint;

#define FAST_OBJ_IMPLEMENTATION
#include <fast_obj.h>

#define CHECK_CLOSESTPOINTINTRIANGLE(VARNAME, T, P, R, EPS)                    \
    do                                                                         \
    {                                                                          \
        Point VARNAME = closestPointInTriangle((T), (P));                      \
        INFO_POINT(VARNAME);                                                   \
        CHECK(glm::distance((R), VARNAME) < (EPS));                            \
    } while (false)

#define CHECK_CLOSESTPOINTINBBOX(VARNAME, B, P, R, EPS)                        \
    do                                                                         \
    {                                                                          \
        Point VARNAME = closestPointInBBox((B), (P));                          \
        INFO_POINT(VARNAME);                                                   \
        CHECK(glm::distance((R), VARNAME) < (EPS));                            \
    } while (false)

#define INFO_POINT(VARNAME) \
    INFO(#VARNAME ":= (" << VARNAME.x << ", " << VARNAME.y << ", " << VARNAME.z << ")")

TEST_CASE("testing closestPointInTriangle with single point triangle")
{
    Point Vs[] = {
        {0.0f, 0.0f, 0.0f},
        {1.0f, 0.432f, 829.0f},
        {10000000000.0f, 0.24f, 0.1f}};

    Point Ps[] = {
        {0.0f, 0.0f, 0.0f},
        {4.23f, 6.15, -43.f},
        {0.15f, -123456789.0f, 342.4f}};

    for (const Point& V : Vs) {
        const Triangle T = {V, V, V};

        for (const Point& P : Ps) {
            SUBCASE("")
            {
                INFO_POINT(V);
                INFO_POINT(P);

                CHECK_CLOSESTPOINTINTRIANGLE(Q, T, P, V, 1.e-6f);
            }
        }
    }
}

TEST_CASE("testing closestPointInTriangle with point in triangle")
{
    for (int i = 0; i < 10; ++i)
    {
        SUBCASE("")
        {
            std::seed_seq seed{i + 0xf25b516aUL};
            std::mt19937 gen(seed);
            std::uniform_real_distribution<> dis(-1000.0f, 1000.0f);

            Point A{dis(gen), dis(gen), dis(gen)};
            Point B{dis(gen), dis(gen), dis(gen)};
            Point C{dis(gen), dis(gen), dis(gen)};
            Triangle T{A, B, C};

            INFO_POINT(A);
            INFO_POINT(B);
            INFO_POINT(C);

            CHECK_CLOSESTPOINTINTRIANGLE(Q, T, A, A, 1.e-6f);
            CHECK_CLOSESTPOINTINTRIANGLE(Q, T, B, B, 1.e-6f);
            CHECK_CLOSESTPOINTINTRIANGLE(Q, T, C, C, 1.e-6f);

            for (int j = 0; j < 10; ++j) {
                SUBCASE("")
                {
                    float u = std::abs(dis(gen));
                    float v = std::abs(dis(gen));
                    float w = std::abs(dis(gen));
                    float s = u + v + w;
                    u /= s;
                    v /= s;
                    w /= s;

                    INFO("(u, v, w) := (" << u << ", " << v << ", " << w << ")");

                    Point P = u * A + v * B + w * C;

                    INFO_POINT(P);

                    CHECK_CLOSESTPOINTINTRIANGLE(Q, T, P, P, 1.e-4f);
                }
            }
        }
    }
}

TEST_CASE("testing closestPointInTriangle double call")
{
    for (int i = 0; i < 100; ++i)
    {
        SUBCASE("")
        {
            std::seed_seq seed{0x7e0ae8cbUL - i * i};
            std::mt19937 gen(seed);
            std::uniform_real_distribution<> dis(-1000.0f, 1000.0f);

            Point A{dis(gen), dis(gen), dis(gen)};
            Point B{dis(gen), dis(gen), dis(gen)};
            Point C{dis(gen), dis(gen), dis(gen)};
            Triangle T{A, B, C};

            INFO_POINT(A);
            INFO_POINT(B);
            INFO_POINT(C);

            Point P{dis(gen), dis(gen), dis(gen)};

            INFO_POINT(P);

            Point P2 = closestPointInTriangle(T, P);

            INFO_POINT(P2);

            CHECK_CLOSESTPOINTINTRIANGLE(Q, T, P, P2, 1.e-6f);
            CHECK_CLOSESTPOINTINTRIANGLE(Q, T, P2, P2, 1.e-6f);
        }
    }
}

TEST_CASE("testing closestPointInBBox with single point BBox")
{
    Point Vs[] = {
        {0.0f, 0.0f, 0.0f},
        {1.0f, 0.432f, 829.0f},
        {100000.0f, 0.24f, 0.1f}};

    Point Ps[] = {
        {0.0f, 0.0f, 0.0f},
        {4.23f, 6.15, -43.f},
        {0.15f, -12345.0f, 342.4f}};

    for (const Point& V : Vs) {
        const BBox B = {V, V};

        for (const Point& P : Ps) {
            SUBCASE("")
            {
                INFO_POINT(V);
                INFO_POINT(P);

                CHECK_CLOSESTPOINTINBBOX(Q, B, P, V, 1.e-6f);
            }
        }
    }
}

TEST_CASE("testing closestPointInBBox with point in bbox")
{
    for (int i = 0; i < 10; ++i)
    {
        SUBCASE("")
        {
            std::seed_seq seed{i + 0xfa57f16aUL};
            std::mt19937 gen(seed);
            std::uniform_real_distribution<> dis(-1000.0f, 1000.0f);

            Point A{dis(gen), dis(gen), dis(gen)};
            Point B{dis(gen), dis(gen), dis(gen)};

            BBox box{glm::min(A, B), glm::max(A, B)};

            INFO_POINT(box.minCoords);
            INFO_POINT(box.maxCoords);

            CHECK_CLOSESTPOINTINBBOX(Q, box, A, A, 1.e-6f);
            CHECK_CLOSESTPOINTINBBOX(Q, box, B, B, 1.e-6f);
            CHECK_CLOSESTPOINTINBBOX(Q, box, box.minCoords, box.minCoords, 1.e-6f);
            CHECK_CLOSESTPOINTINBBOX(Q, box, box.maxCoords, box.maxCoords, 1.e-6f);

            for (int j = 0; j < 10; ++j) {
                SUBCASE("")
                {
                    Point S = glm::abs(Point{dis(gen), dis(gen), dis(gen)})/1000.0f;
                    
                    INFO_POINT(S);

                    Point P = glm::mix(box.minCoords, box.maxCoords, S);

                    INFO_POINT(P);

                    CHECK_CLOSESTPOINTINBBOX(Q, box, P, P, 1.e-6f);
                }
            }
        }
    }
}

TEST_CASE("testing closestPointInBBox double call")
{
    for (int i = 0; i < 100; ++i)
    {
        SUBCASE("")
        {
            std::seed_seq seed{0xae45f8cbUL - i * i};
            std::mt19937 gen(seed);
            std::uniform_real_distribution<> dis(-1000.0f, 1000.0f);

            Point A{dis(gen), dis(gen), dis(gen)};
            Point B{dis(gen), dis(gen), dis(gen)};

            BBox box{glm::min(A, B), glm::max(A, B)};

            INFO_POINT(box.minCoords);
            INFO_POINT(box.maxCoords);

            Point P{dis(gen), dis(gen), dis(gen)};
            INFO_POINT(P);

            Point P2 = closestPointInBBox(box, P);
            INFO_POINT(P2);

            CHECK_CLOSESTPOINTINBBOX(Q, box, P, P2, 1.e-6f);
            CHECK_CLOSESTPOINTINBBOX(Q, box, P2, P2, 1.e-6f);
        }
    }
}


TEST_CASE("testing ClosestPointInMesh empty mesh")
{
    std::vector<Triangle> mesh;
    ClosestPointInMesh query(mesh);

    Point P;
    CHECK(!query(P));
}

TEST_CASE("testing ClosestPointInMesh single triangle")
{
    const Triangle T = {
        Point{0.0f, 0.0f, 0.0f},
        Point{1.0f, 0.0f, 0.0f},
        Point{0.0f, 1.0f, 0.0f}};

    std::vector<Triangle> mesh = {T};
    ClosestPointInMesh query(mesh);

    Point P = {0.32141f, 0.2418f, 15.f};

    auto R = query(P);
    CHECK(R.has_value());
    CHECK_CLOSESTPOINTINTRIANGLE(Q, T, P, R.value(), 1.e-6);
}

TEST_CASE("testing ClosestPointInMesh")
{
    std::string models[] = {
        "bunny.obj",
        "buddha.obj"
        };

    for (const auto& model : models) {
        SUBCASE(model.c_str())
        {
            std::unique_ptr<fastObjMesh, void(*)(fastObjMesh*)> mesh { fast_obj_read(model.c_str()), fast_obj_destroy };

            std::vector<Triangle> triangles;
            for (unsigned i = 0; i < mesh->group_count; ++i)
            {
                const fastObjGroup& grp = mesh->groups[i];

                int idx = 0;
                for (unsigned j = 0; j < grp.face_count; ++j)
                {
                    unsigned fv = mesh->face_vertices[grp.face_offset + j];

                    int start_idx = idx;
                    for (unsigned k = 1; k < (fv-1); ++k) {
                        fastObjIndex mi0 = mesh->indices[grp.index_offset + start_idx];
                        fastObjIndex mik = mesh->indices[grp.index_offset + start_idx + k];
                        fastObjIndex mikp1 = mesh->indices[grp.index_offset + start_idx + k + 1];

                        Point p0 = { mesh->positions[3 * mi0.p + 0], mesh->positions[3 * mi0.p + 1], mesh->positions[3 * mi0.p + 2] };
                        Point pk = { mesh->positions[3 * mik.p + 0], mesh->positions[3 * mik.p + 1], mesh->positions[3 * mik.p + 2] };
                        Point pkp1 = { mesh->positions[3 * mikp1.p + 0], mesh->positions[3 * mikp1.p + 1], mesh->positions[3 * mikp1.p + 2] };

                        Triangle triangle = {p0, pk, pkp1};
                        triangles.emplace_back(triangle);
                    }

                    idx += fv;
                }
            }

            ClosestPointInMesh query(triangles);

            for (int i = 0; i < 10; ++i) {
                SUBCASE("")
                {
                    std::seed_seq seed{0xf45adc78UL* (1 + i * i)};
                    std::mt19937 gen(seed);
                    std::uniform_real_distribution<> dis(-1000.0f, 1000.0f);

                    Point P{dis(gen), dis(gen), dis(gen)};
                    
                    auto cp = query(P);
                    CHECK(cp.has_value());

                    Point Q;
                    float minDistance = INFINITY;
                    for (auto & triangle : triangles) {
                        Point p = closestPointInTriangle(triangle, P);
                        float distance = glm::distance(p, P);
                        if (distance < minDistance)
                        {
                            Q = p;
                            minDistance = distance;
                        }
                    }

                    CHECK(std::abs(glm::distance(cp.value(), P) - minDistance) < 1.e-4);
                }
            }
        }
    }
}
